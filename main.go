package main

import (
	"flag"
	"image"
	"image/color"
	"image/png"
	"os"
	"strings"
	"sync"

	"github.com/pbnjay/pixfont"
)

var wg sync.WaitGroup

func main() {
	pngs := flag.Bool("png", false, "Save pngs too")
	gos := flag.Bool("go", false, "Save go class too")
	alphabet := "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz ./\\*\"'!?;:,+-()[]{}@&#<>=~^°%÷ÁáÉéÖöÜüÓóÍíÚúŰűŐő0123456789π"
	flag.Parse()
	if *pngs {
		for _, l := range alphabet {
			wg.Add(1)
			go genLetter(string(l))
		}
	}
	if *gos {
		for _, l := range alphabet {
			wg.Add(1)
			genGoFile(string(l))
		}
		classText += "}"
		f, _ := os.OpenFile("out/go/letters.go", os.O_CREATE|os.O_RDWR, 0644)
		defer f.Close()
		f.WriteString(classText)
	}
	wg.Wait()
}

func genLetter(l string) {
	img := image.NewRGBA(image.Rect(0, 0, 8, 8))

	for x := 0; x < 8; x++ {
		for y := 0; y < 8; y++ {
			img.Set(x, y, color.White)
		}
	}

	pixfont.DrawString(img, 0, 0, l, color.Black)

	f, _ := os.OpenFile("out/png/"+l+".png", os.O_CREATE|os.O_RDWR, 0644)
	defer f.Close()
	png.Encode(f, img)
	wg.Done()
}

var classText = `
package letters

var Letters = map[string][8][8]bool{
`

func genGoFile(l string) {
	img := image.NewRGBA(image.Rect(0, 0, 8, 8))
	for x := 0; x < 8; x++ {
		for y := 0; y < 8; y++ {
			img.Set(x, y, color.White)
		}
	}
	pixfont.DrawString(img, 0, 0, l, color.Black)
	wg.Done()
	var s strings.Builder
	s.WriteString("\"")
	switch l {
	case "\"", "\\":
		s.WriteString("\\")
		s.WriteString(l)
		break
	default:
		s.WriteString(l)
		break
	}
	s.WriteString("\": [8][8]bool{\n")
	for x := 0; x < 8; x++ {
		s.WriteString("{")
		for y := 0; y < 8; y++ {
			if r, g, b, _ := img.At(x, y).RGBA(); r > 10 && g > 10 && b > 10 {
				s.WriteString("false, ")
			} else {
				s.WriteString("true, ")
			}
		}
		s.WriteString("},\n")
	}
	s.WriteString("},\n")
	classText += s.String()
}
